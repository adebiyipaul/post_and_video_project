<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

// Posts

Route::post('post/create', 'PostController@create');
Route::post('post/list', 'PostController@list');
Route::post('post/show', 'PostController@show');
Route::post('post/update', 'PostController@update');
Route::post('post/delete', 'PostController@delete');
Route::post('post/listComments', 'PostController@listComments');

// Videos

Route::post('video/create', 'VideoController@create');
Route::post('video/list', 'VideoController@list');
Route::post('video/show', 'VideoController@show');
Route::post('video/update', 'VideoController@update');
Route::post('video/delete', 'VideoController@delete');
Route::post('video/listComments', 'VideoController@listComments');

// Comments

Route::post('comment/create', 'CommentController@create');
Route::post('comment/update', 'CommentController@update');
Route::post('comment/delete', 'CommentController@destroy');
