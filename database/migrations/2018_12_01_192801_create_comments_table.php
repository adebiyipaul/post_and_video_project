<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned()->index()->nullable();
            $table->foreign('post_id')->references('id')->on('posts');
            $table->integer('video_id')->unsigned()->index()->nullable();
            $table->foreign('video_id')->references('id')->on('videos');
            $table->enum('category', array('Post', 'Video'));
            $table->string('comment_title');
            $table->longText('comment_body');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
