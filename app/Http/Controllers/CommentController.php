<?php

namespace App\Http\Controllers;

use App\Services\CommentService;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    protected $commentService;

    /**
     * StationController constructor.
     * @param CommentService $commentService
     */
    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $postData = [
            'post_id' => $request['post_id'],
            'video_id' => $request['video_id'],
            'category' => $request['category'],
            'comment_title' => $request['comment_title'],
            'comment_body' => $request['comment_body'],
        ];

        return $this->commentService->create($postData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request)
    {
        $postData = [
            'post_id' => $request['post_id'],
            'video_id' => $request['video_id'],
            'category' => $request['category'],
            'comment_title' => $request['comment_title'],
            'comment_body' => $request['comment_body'],
            'comment_id' => $request['comment_id'],
        ];

        return $this->commentService->update($postData);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        return $this->commentService->delete($request['comment_id']);
    }
}
