<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 18/11/2018
 * Time: 3:24 PM
 */

namespace App\Repositories;

use App\Models\Comment;

class CommentRepository extends BaseRepository
{
    public function model()
    {
        return Comment::class;
    }
}
