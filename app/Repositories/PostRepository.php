<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 18/11/2018
 * Time: 3:24 PM
 */

namespace App\Repositories;

use App\Models\Post;

class PostRepository extends BaseRepository
{
    public function model()
    {
        return Post::class;
    }

    public function showComments($post_id)
    {

        $posts = Post::find($post_id);
        $posts->load(['comments' => function ($q) {
            $q->orderBy('id', 'desc');
        }]);

        return $posts->comments;
    }
}
