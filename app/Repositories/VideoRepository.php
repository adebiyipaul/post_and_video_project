<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 18/11/2018
 * Time: 3:24 PM
 */

namespace App\Repositories;

use App\Models\Video;

class VideoRepository extends BaseRepository
{
    public function model()
    {
        return Video::class;
    }

    public function showComments($video_id)
    {

        $videos = Video::find($video_id);
        $videos->load(['comments' => function ($q) {
            $q->orderBy('id', 'desc');
        }]);

        return $videos->comments;
    }
}
