<?php
namespace App\Services;

use App\Repositories\CommentRepository;

class CommentService
{
    protected $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($data)
    {
        $result = $this->commentRepository->create($data);

        $success['data'] = $result;
        $success['message'] =  'Comment was created successfully';
        $success['statusCode'] = 200;
        return response()->json(['error'=>$success], $success['statusCode']);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($data)
    {
        $result = $this->commentRepository->updateById($data['post_id'], $data);
        $success['data'] =  $result;
        $success['message'] =  'Comment updated successfully';
        $success['statusCode'] = 200;
        return response()->json(['success' => $success], $success['statusCode']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        if($this->commentRepository->deleteById($id))
        {
            $success['message'] =  'Comment deleted successfully';
            $success['statusCode'] = 200;
            return response()->json(['success' => $success], $success['statusCode']);
        }
        else
        {
            $error['message'] =  'Error deleting comment';
            $error['statusCode'] = 401;
            return response()->json(['error'=>$error], $error['statusCode']);
        }
    }
}
