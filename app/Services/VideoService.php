<?php
namespace App\Services;

use App\Repositories\VideoRepository;

class VideoService
{
    protected $videoRepository;

    public function __construct(VideoRepository $videoRepository)
    {
        $this->videoRepository = $videoRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $result =  $this->videoRepository->get();
        $success['data'] =  $result;
        $success['message'] =  'Post fetched successfully';
        $success['statusCode'] = 200;
        return response()->json(['success' => $success], $success['statusCode']);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function listComments($data)
    {
        $result =  $this->videoRepository->showComments($data);
        $success['data'] =  $result;
        $success['message'] =  'Comment fetched successfully';
        $success['statusCode'] = 200;
        return response()->json(['success' => $success], $success['statusCode']);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($data)
    {
        $result = $this->videoRepository->create($data);

        $success['data'] = $result;
        $success['message'] =  'Post was created successfully';
        $success['statusCode'] = 200;
        return response()->json(['error'=>$success], $success['statusCode']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $result = $this->videoRepository->getById($id);

        $success['data'] =  $result;
        $success['message'] =  'Post fetched successfully';
        $success['statusCode'] = 200;
        return response()->json(['success' => $success], $success['statusCode']);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($data)
    {
        $result = $this->videoRepository->updateById($data['post_id'], $data);
        $success['data'] =  $result;
        $success['message'] =  'post updated successfully';
        $success['statusCode'] = 200;
        return response()->json(['success' => $success], $success['statusCode']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        if($this->videoRepository->deleteById($id))
        {
            $success['message'] =  'Post deleted successfully';
            $success['statusCode'] = 200;
            return response()->json(['success' => $success], $success['statusCode']);
        }
        else
        {
            $error['message'] =  'Error deleting post';
            $error['statusCode'] = 401;
            return response()->json(['error'=>$error], $error['statusCode']);
        }
    }
}
