<?php
namespace App\Services;

use App\Repositories\PostRepository;

class PostService
{
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $result =  $this->postRepository->get();
        $success['data'] =  $result;
        $success['message'] =  'Post fetched successfully';
        $success['statusCode'] = 200;
        return response()->json(['success' => $success], $success['statusCode']);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function listComments($data)
    {
        $result =  $this->postRepository->showComments($data);
        $success['data'] =  $result;
        $success['message'] =  'Comment fetched successfully';
        $success['statusCode'] = 200;
        return response()->json(['success' => $success], $success['statusCode']);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($data)
    {
        $result = $this->postRepository->create($data);

        $success['data'] = $result;
        $success['message'] =  'Post was created successfully';
        $success['statusCode'] = 200;
        return response()->json(['error'=>$success], $success['statusCode']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $result = $this->postRepository->getById($id);

        $success['data'] =  $result;
        $success['message'] =  'Post fetched successfully';
        $success['statusCode'] = 200;
        return response()->json(['success' => $success], $success['statusCode']);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($data)
    {
        $result = $this->postRepository->updateById($data['post_id'], $data);
        $success['data'] =  $result;
        $success['message'] =  'post updated successfully';
        $success['statusCode'] = 200;
        return response()->json(['success' => $success], $success['statusCode']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        if($this->postRepository->deleteById($id))
        {
            $success['message'] =  'Post deleted successfully';
            $success['statusCode'] = 200;
            return response()->json(['success' => $success], $success['statusCode']);
        }
        else
        {
            $error['message'] =  'Error deleting post';
            $error['statusCode'] = 401;
            return response()->json(['error'=>$error], $error['statusCode']);
        }
    }
}
