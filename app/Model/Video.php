<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //
    protected $fillable = [
        'video_name',
        'video_path'
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
