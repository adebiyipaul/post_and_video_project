<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable = [
        'post_id',
        'video_id',
        'category',
        'comment_title',
        'comment_body'
    ];

    public function posts_and_videos()
    {
        return $this->belongsTo(Comment::class);
    }
}
